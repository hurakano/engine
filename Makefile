.PHONY: clean

FLAGS=--std=c++11 -Iinclude/
SRC=$(wildcard src/*.cpp)
OBJ=$(patsubst src/%.cpp,obj/%.o,$(SRC))
LIB=-lsfml-graphics -lsfml-window -lsfml-system

main:main.cpp $(OBJ)
	g++ -o $@ $^ $(FLAGS) $(LIB)
	
obj/%.o:src/%.cpp include/%.hpp
	g++ -o $@ $< $(FLAGS) -c
	
clean:
	rm obj/* main

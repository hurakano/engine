#ifndef FIGURE_HPP
#define FIGURE_HPP

#include<vector>

#include<SFML/Graphics/Drawable.hpp>
#include<SFML/Graphics/RenderTarget.hpp>
#include<SFML/Graphics/RenderStates.hpp>
#include<SFML/Graphics/Transformable.hpp>
#include<SFML/Graphics/Rect.hpp>
#include<SFML/System/Vector2.hpp>

#include<Line.hpp>
#include<SceneObject.hpp>

class Character: public SceneObject
{
public:

	Character();
	virtual ~Character(){};
	
	
	void update(float deltaTime);
	void onOverlap
		(SceneObject *otherObject, SceneComponent *otherComponent, SceneComponent *ownedComponent);
	
protected:
	
	virtual void handleMovement(){};
	
};

#endif//FIGURE_HPP

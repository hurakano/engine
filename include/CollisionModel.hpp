#ifndef COLLISION_MODEL_HPP
#define COLLISION_MODEL_HPP

#include<SceneComponent.hpp>
#include<list>

class CollisionModel
{
public:
	
	virtual void preCollisionCheck(SceneComponent *obj){};
	virtual void applyCollision(SceneComponent *obj1, SceneComponent *obj2){};
	virtual void postCollisionCheck(SceneComponent *obj){};
	
	void performCollisionCheck(float deltaTime);
	void assignComponentsList(std::list<SceneComponent*> *pointer);
	
	
	mutable float deltaTime;
	
private:
	
	std::list<SceneComponent*> *sceneComponents;

};

#endif//COLLISION_MODEL_HPP

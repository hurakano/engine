#ifndef LINE_HPP
#define LINE_HPP

#include<SFML/System/Vector2.hpp>
#include<SFML/Graphics/Rect.hpp>

#include<SceneComponent.hpp>

class Line: public SceneComponent
{
public:
	Line();//default constructor
	Line(sf::Vector2f p1, sf::Vector2f p2);
	Line(float x1, float y1, float x2, float y2);
	Line(const Line &obj);
	
	
	//if two lines intersects
	bool intersects(const Line &line)const;
	//get line angle to OX
	float angle_OX()const;
	
	void draw(sf::RenderTarget &target, sf::RenderStates states)const;
	sf::FloatRect getBounds()const{return sf::FloatRect();};
	
	sf::Vector2f a;//points defining line
	sf::Vector2f b;
};

#endif//LINE_HPP

#ifndef OBJECT_HPP
#define OBJECT_HPP

#include<string>

class World;

class Object
{
public:
	
	Object();
	virtual ~Object(){};

	virtual void update(float deltaTime){};

	virtual void onCreate(){};
	virtual void onDestroy(){};
	
	virtual void destroy();
	
	void setName(std::string name){objectName = name;};
	std::string getName(){return objectName;};
	
protected:

	bool updateFlag;
	bool toDestroy;
	
	std::string objectName;
	
	friend World;
};

#endif//OBJECT_HPP

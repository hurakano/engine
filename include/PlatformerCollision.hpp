#ifndef PLATFORMER_COLLISION_HPP
#define PLATFORMER_COLLISION_HPP

#include<CollisionModel.hpp>

class PlatformerCollision: public CollisionModel
{
public:

	virtual void preCollisionCheck(SceneComponent *obj);
	virtual void applyCollision(SceneComponent *obj1, SceneComponent *obj2);
	virtual void postCollisionCheck(SceneComponent *obj);
};

#endif//PLATFORMER_COLLISION_HPP

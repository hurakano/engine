#ifndef PLAYER_HPP
#define PLAYER_HPP

#include<Character.hpp>

class Player:public Character
{
	~Player(){};
private:
	
	virtual void handleMovement();
};

#endif//PLAYER_HPP

//scene is a class that contains other and manage drawing and displaying

#ifndef SCENE_HPP
#define SCENE_HPP

#include<SFML/Graphics/RenderWindow.hpp>
#include<list>

#include<World.hpp>
#include<Object.hpp>
#include<Line.hpp>
#include<SceneObject.hpp>
#include<SceneComponent.hpp>

class Character;

class Scene:public Object
{
public:

	Scene(World *wrld);
	
	virtual void destroy();
	
//adding object to scene
	
	template<class objType>
	objType *spawnObject();
	
	//scene objects
	std::list<SceneObject*> sceneObjects;
	
	World *getWorld(){return world;};
	
private:
	
	World *world;
};

template<class objType>
objType *Scene::spawnObject()
{
	objType *object = new objType();//create new object of type
	sceneObjects.push_back(object);
	object->assignToScene(this);
	world->sceneObjects.push_back(object);
	object->onCreate();
	return object;
}

#endif//SCENE_HPP

#ifndef SCENE_COMPONENT_HPP
#define SCENE_COMPONENT_HPP

#include<SFML/Graphics/Drawable.hpp>
#include<SFML/Graphics/Transformable.hpp>
#include<SFML/Graphics/Rect.hpp>
#include<list>

#include<Object.hpp>


class Scene;
class SceneObject;

class SceneComponent:public Object, public sf::Drawable, public sf::Transformable
{
public:

	SceneComponent();
	virtual ~SceneComponent(){};
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states)const=0;
	
	virtual sf::FloatRect getBounds()const=0;
	sf::Transform getTransform()const;
	
	void registerComponent();
	void unregisterComponent();
	
	void addChild(SceneComponent *comp);
	
	SceneObject *owningObject;
	SceneComponent *parentComponent;
	std::list<SceneComponent*> children;
	
	SceneComponent *getRoot()const;
	
	bool toDraw;
	bool overlapping;
	bool blocking;
	
	sf::Vector2f force;
	sf::Vector2f getForce();
	void modifyForce(sf::Vector2f force);
};


#endif//SCENE_COMPONENT_HPP

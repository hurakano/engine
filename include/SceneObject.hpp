#ifndef SCENE_OBJECT_HPP
#define SCENE_OBJECT_HPP

#include<SFML/Graphics/Drawable.hpp>
#include<SFML/Graphics/Transformable.hpp>
#include<SFML/Graphics/Rect.hpp>
#include<SFML/System/Vector2.hpp>
#include<list>

#include<Object.hpp>
#include<SceneComponent.hpp>

class Scene;

class SceneObject:public Object
{
public:
	
	SceneObject();
	virtual ~SceneObject(){};
	
	virtual void destroy();
	
	void assignToScene(Scene *scn){ scene = scn; };
	
//function for physics n stuff
	virtual void onOverlap
		(SceneObject *otherObject, SceneComponent *otherComponent, SceneComponent *ownedComponent){};

	bool physicsObject;
	
	
//variables
	
	SceneComponent *rootComponent;
	std::list<SceneComponent*> sceneComponents;
	void addSceneComponent(SceneComponent *scnComp);
	void removeSceneComponent(SceneComponent *scnComp);
	
	Scene *getScene()const;
	World *getWorld()const;
	
private:
	Scene *scene;//in which scene object exists

	

};

#endif//SCENE_OBJECT_HPP

#ifndef SPRITE_COMPONENT_HPP
#define SPRITE_COMPONENT_HPP

#include<SFML/Graphics/Sprite.hpp>
#include<SFML/Graphics/Texture.hpp>

#include<string>

#include<SceneComponent.hpp>

class SpriteComponent:public SceneComponent
{
public:
	
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states)const;
	
	void openTexture(std::string filename);
	
	virtual sf::FloatRect getBounds()const;
	
protected:

	sf::Texture texture;
	sf::Sprite sprite;

};
#endif//SPRITE_COMPONENT_HPP

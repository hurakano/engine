#ifndef WORLD_HPP
#define WORLD_HPP

#include<SFML/Graphics/RenderWindow.hpp>
#include<list>

#include<Object.hpp>
#include<CollisionModel.hpp>

class Scene;
class SceneObject;
class SceneComponent;

class World
{
public:

	World();
	virtual ~World();
	
	void mainWorldLoop();
	void quit();
	
	virtual void onGameStart();
	
	void setCollisionModel(CollisionModel *cm){collisionModel = cm;};
	

	void destroyScene(Scene *scn);
	void destroySceneObject(SceneObject *obj);
	void destroySceneComponent(SceneComponent *comp);
	
	Scene *createScene();
	
	std::list<Scene*> scenes;
	std::list<SceneObject*> sceneObjects;//to scene objects to update
	std::list<SceneComponent*> sceneComponents;//to draw and collission
	
private: 
	
	void checkCollisions();
	
	void updateObjects(float timeInterval);
	
	void render();
	void garbageCollect();
	
	sf::RenderWindow *window;
	bool endGame;
	
	CollisionModel *collisionModel;
};

#endif//WORLD_HPP

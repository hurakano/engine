#include<World.hpp>
#include<PlatformerCollision.hpp>

using namespace sf;

int main()
{
	
	World world;
	PlatformerCollision plCol;
	
	world.setCollisionModel(&plCol);
	world.mainWorldLoop();
	
	return 0;
}
////////////////////////////////////////////////////////

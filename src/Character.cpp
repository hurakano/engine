#include<Character.hpp>
#include<SFML/Graphics/RectangleShape.hpp>


#include<cmath>

#include<Scene.hpp>
#include<World.hpp>

#define PI 3.141592653

using namespace sf;

Character::Character()
{
	//empty
}
//////////////////////////////////////////////////////////////

void Character::onOverlap
(SceneObject *otherObject, SceneComponent *otherComponent, SceneComponent *ownedComponent)
{
	getWorld()->quit();
}

////////////////////////////////////////////////////////////////////////////////

void Character::update(float deltaTime)
{
	handleMovement();
	rootComponent->force *= 1000*deltaTime;
}

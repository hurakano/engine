#include<CollisionModel.hpp>

void CollisionModel::performCollisionCheck(float deltaTime)
{
	this->deltaTime = deltaTime;
	
	for(auto x: *sceneComponents)
		preCollisionCheck(x);
		
	for(auto x: *sceneComponents)
		for(auto y: *sceneComponents)
			if(x->blocking && y->blocking && x != y)
				applyCollision(x, y);
				
	for(auto x: *sceneComponents)
		postCollisionCheck(x);
}
////////////////////////////////////////////////////////////////////////

void CollisionModel::assignComponentsList(std::list<SceneComponent*> *pointer)
{
	sceneComponents = pointer;
}
///////////////////////////////////////////////////////////////////

#include<Line.hpp>
#include<cmath>

#include<SFML/Graphics.hpp>//tmp

Line::Line(): Line(sf::Vector2f(0, 0), sf::Vector2f(0, 0))
{ 
	//empty body
}
////////////////////////////////////////////////////////////

Line::Line(sf::Vector2f p1, sf::Vector2f p2): a(p1), b(p2)
{
	overlapping = 0;
	toDraw = 1;
	blocking = 1;
}
////////////////////////////////////////////////////////////

Line::Line(float x1, float y1, float x2, float y2): Line(sf::Vector2f(x1, y1), sf::Vector2f(x2, y2))
{
	//empty body
}
//////////////////////////////////////////////////////////

Line::Line(const Line &obj): Line(obj.a, obj.b)
{
	//empty body
}
///////////////////////////////////////////////////////////
//check if two lines intersects

bool Line::intersects(const Line &line)const
{
	float A, B;
	
	A = ((line.b.x - line.a.x) * (this->a.y - line.a.y ) - 
		(line.b.y - line.a.y) * (this->a.x - line.a.x ))
		/
		((line.b.y - line.a.y) * (this->b.x - this->a.x) - 
		(line.b.x - line.a.x) * (this->b.y - this->a.y));
		
	B = ((this->b.x - this->a.x) * (this->a.y - line.a.y ) - 
		(this->b.y - this->a.y) * (this->a.x - line.a.x ))
		/
		((line.b.y - line.a.y) * (this->b.x - this->a.x) - 
		(line.b.x - line.a.x) * (this->b.y - this->a.y));
		
		
	if(A >= 0  &&  A <= 1  &&  B >= 0  &&  B <= 1)
		return true;
	else
		return false;
}
/////////////////////////////////////////////////////////////

float Line::angle_OX()const
{
	return atan((a.y-b.y) / (a.x-b.x));
}
///////////////////////////////////////////////////////////////

void Line::draw(sf::RenderTarget &target, sf::RenderStates states)const
{
	sf::VertexArray line(sf::Lines, 2);
	
	line[0].position = a;
	line[1].position = b;
	
	target.draw(line);
}

#include<PlatformerCollision.hpp>

#include<Line.hpp>

#include<SFML/System/Vector2.hpp>
#include<cmath>

#define PI 3.141592653

void PlatformerCollision::preCollisionCheck(SceneComponent *obj)
{
	if(!obj->parentComponent)
		obj->force += sf::Vector2f(0, 100*deltaTime);
}
//////////////////////////////////////////////////////////

void PlatformerCollision::applyCollision(SceneComponent *obj1, SceneComponent *obj2)
{
	if(dynamic_cast<Line*>(obj1) != nullptr)return;
	Line *line = dynamic_cast<Line*>(obj2);
	if(line == nullptr)return;
	SceneComponent *obj = obj1;	


	sf::Vector2f force = obj->getForce();
	float height = obj->getBounds().height;
	float width = obj->getBounds().width;
	sf::Vector2f position = obj->getPosition();
	
	obj->setRotation(0);
	float dif = 3;
	int haveGround = 0;
	
//bottom bound
	
	if(force.y > 0)
	{
		Line forceLine(position.x+dif, position.y+height/2, 
			position.x+force.x, position.y+height/2+force.y);
		Line forceLine2(position.x-dif, position.y+height/2, 
			position.x+force.x, position.y+height/2+force.y);
			
	
		if(line->intersects(forceLine) || line->intersects(forceLine2))
		{	
			float angle = line->angle_OX();
			float rot = angle < 0? PI/2.-std::abs(angle) : -(PI/2.-angle);
				
			//gravity
			float x = -force.y*sin(rot)*sin(std::abs(angle));
			float y = force.y*cos(rot)*sin(std::abs(angle));
				
			//horizontal movement
			x += force.x*cos(angle);
			y += force.x*sin(angle);
				
			obj->force = sf::Vector2f(x, y);
			
			obj->setRotation(angle*(180/PI));
			haveGround = 1;
			
			
		}
	}
	
	force = obj->getForce();
	
//left and right bound

	if(force.x != 0)
	{
		float displacment = force.x > 0? width/2 : -width/2;
		Line forceLine = Line(position.x+displacment, position.y+dif, 
			position.x+force.x+displacment, position.y+force.y);
		Line forceLine2 = Line(position.x+displacment, position.y-dif, 
			position.x+force.x+displacment, position.y+force.y);
		
		if(line->intersects(forceLine) || line->intersects(forceLine2))
		{	
			obj->force.x = 0;
			obj->modifyForce(sf::Vector2f(-obj->getForce().x, 0));
		}
	}
	
	force = obj->getForce();
	
//upper bound

	if(force.y < 0)
	{
		Line forceLine = Line(position.x+dif, position.y-height/2, 
			position.x+force.x, position.y+force.y-height/2);
		Line forceLine2 = Line(position.x-dif, position.y-height/2, 
			position.x+force.x, position.y+force.y+height/2);
		
		if(line->intersects(forceLine) || line->intersects(forceLine2))
		{	
			obj->force.y = 0;
			obj->modifyForce(sf::Vector2f(0, -obj->getForce().y));
		}
		
	}
}
///////////////////////////////////////////////////////////////

void PlatformerCollision::postCollisionCheck(SceneComponent *obj)
{
	if(!obj->parentComponent)
	{
		obj->move(obj->force);
		obj->force = sf::Vector2f();
	}
}

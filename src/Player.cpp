#include<Player.hpp>
#include<SFML/Window/Keyboard.hpp>
#include<SFML/System/Vector2.hpp>

using namespace sf;

void Player::handleMovement()
{
	if(Keyboard::isKeyPressed(Keyboard::Right))
		rootComponent->force += Vector2f(0.1, 0);
	if(Keyboard::isKeyPressed(Keyboard::Left))
		rootComponent->force += Vector2f(-0.1, 0);
	if(Keyboard::isKeyPressed(Keyboard::Up))
		rootComponent->force += Vector2f(0, -0.2);
}

#include<SceneComponent.hpp>

#include<SceneObject.hpp>
#include<Scene.hpp>
#include<World.hpp>

SceneComponent::SceneComponent()
{
	parentComponent = nullptr;
	owningObject = nullptr;
	toDraw = 1;
	overlapping = 1;
	blocking = 1;
}
/////////////////////////////////////////////

void SceneComponent::registerComponent()
{
	owningObject->getWorld()->sceneComponents.push_back(this);
}
/////////////////////////////////////////////////////////

void SceneComponent::unregisterComponent()
{
	owningObject->getWorld()->sceneComponents.remove(this);
}
//////////////////////////////////////////////////

void SceneComponent::addChild(SceneComponent *comp)
{
	children.push_back(comp);
	comp->parentComponent = this;
	comp->owningObject = owningObject;
}
////////////////////////////////////////////////////////

SceneComponent *SceneComponent::getRoot()const
{
	return owningObject->rootComponent;
}
/////////////////////////////////////////////////////////////

sf::Transform SceneComponent::getTransform()const
{	
	
	sf::Transform transform = sf::Transformable::getTransform();
	if(parentComponent)
		transform *= parentComponent->getTransform();
	
	return transform;
}
////////////////////////////////////////////////////////

sf::Vector2f SceneComponent::getForce()
{
	sf::Vector2f returnForce = force;
	if(parentComponent)
		returnForce += parentComponent->getForce();
	return returnForce;
}
///////////////////////////////////////////////////

void SceneComponent::modifyForce(sf::Vector2f force)
{
	this->force += force;
	if(parentComponent)
		parentComponent->modifyForce(force);
}

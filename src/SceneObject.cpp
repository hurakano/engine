#include<SceneObject.hpp>
#include<Scene.hpp>

#include<cmath>

SceneObject::SceneObject()
{
	physicsObject = 1;
}
///////////////////////////////////////////////////////////////

void SceneObject::destroy()
{
	Object::destroy();
}
//////////////////////////////////////////////////////////////////

void SceneObject::addSceneComponent(SceneComponent *scnComp)
{
	sceneComponents.push_back(scnComp);
	scnComp->owningObject = this;
}
///////////////////////////////////////////////////////////////////

void SceneObject::removeSceneComponent(SceneComponent *scnComp)
{
	sceneComponents.remove(scnComp);
	scnComp->owningObject = nullptr;
}
////////////////////////////////////////////////////

Scene *SceneObject::getScene()const
{
	return scene;
}
/////////////////////////////////////////////////////

World *SceneObject::getWorld()const
{
	return getScene()->getWorld();
}

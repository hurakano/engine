#include<SpriteComponent.hpp>

#include<SceneObject.hpp>

#include<SFML/Graphics/RenderTarget.hpp>

void SpriteComponent::draw(sf::RenderTarget &target, sf::RenderStates states)const
{
	states.transform *= getTransform();
	target.draw(sprite, states);
}
//////////////////////////////////////////////////////////

void SpriteComponent::openTexture(std::string filename)
{
	texture.loadFromFile(filename);
	sprite.setTexture(texture);
	sprite.setOrigin(sprite.getLocalBounds().width/2, sprite.getLocalBounds().height/2);
}
////////////////////////////////////////////////////////

sf::FloatRect SpriteComponent::getBounds()const
{
	sf::Transform transform = getTransform();
	return transform.transformRect(sprite.getLocalBounds());
}
//////////////////////////////////////////////////////////////////

#include<World.hpp>

#include<Scene.hpp>
#include<Line.hpp>
#include<Character.hpp>
#include<Scene.hpp>
#include<Player.hpp>
#include<SpriteComponent.hpp>


#include<SFML/System/Clock.hpp>
#include<SFML/Window/Event.hpp>

#define FRAMERATE 700.

World::World()
{
	window = new sf::RenderWindow(sf::VideoMode(500, 500), "Test");
	endGame = 0;
}
/////////////////////////////////////////////////////////

World::~World()
{
	while(!scenes.empty())
		destroyScene(scenes.front());
	
	window->close();
	delete window;
}
///////////////////////////////////////////////////////

void World::quit()
{
	endGame = 1;
}

////////////////////////////////////////////////////////

void World::destroySceneComponent(SceneComponent *comp)
{
	comp->onDestroy();
	
	while(!comp->children.empty())
		destroySceneComponent(comp->children.front());
	
	if(comp->parentComponent)
		comp->parentComponent->children.remove(comp);
		
	comp->owningObject->sceneComponents.remove(comp);
	comp->unregisterComponent();
	delete comp;
}

/////////////////////////////////////////////////////////////

void World::destroySceneObject(SceneObject *obj)
{
	obj->onDestroy();
	
	while(!obj->sceneComponents.empty())
		destroySceneComponent(obj->sceneComponents.front());
	
	sceneObjects.remove(obj);
	obj->getScene()->sceneObjects.remove(obj);
	delete obj;
}
////////////////////////////////////////////////////////

void World::destroyScene(Scene *scn)
{
	scn->onDestroy();
	
	while(!scn->sceneObjects.empty())//iterate over all sceneObjects
		destroySceneObject(scn->sceneObjects.front());//destroy object
		
	scenes.remove(scn);
	delete scn;
}
////////////////////////////////////////////////////////////

Scene *World::createScene()
{
	Scene *scene = new Scene(this);
	scenes.push_back(scene);
	scene->onCreate();
	return scene;
}

///////////////////////////////////////////////////////////////////////

void World::mainWorldLoop()
{
	collisionModel->assignComponentsList(&sceneComponents);
	onGameStart();
	
	window->setFramerateLimit(120);
	sf::Clock frameClock;
	frameClock.restart();
	
	while(window->isOpen() && !endGame)
	{
		sf::Event event;
		
		float lastFrameDuration = frameClock.restart().asSeconds();
		
		if(window->pollEvent(event))
		{
			if(event.type == sf::Event::Closed)
				quit();
		}

//update objects
		updateObjects(lastFrameDuration);

		collisionModel->performCollisionCheck(lastFrameDuration);
		
		checkCollisions();
				
//remove objects to destroy
		
		garbageCollect();
			
//draw frame
		render();
	}
}

//////////////////////////////////////////////////////////////////////////////////////

void World::checkCollisions()
{	
	for(auto x: sceneComponents)
		for(auto y: sceneComponents)
		{
			if(x == y)continue;//same object
			if(!x->overlapping || !y->overlapping)continue;
		
			if(x->getBounds().intersects(y->getBounds()))
				x->owningObject->onOverlap(y->owningObject, y, x);
		}
}
///////////////////////////////////////////////////////////////////////////////////////


void World::updateObjects(float timeInterval)
{
	for(auto x: scenes)
		if(x->updateFlag)
			x->update(timeInterval);
			
	for(auto x: sceneObjects)
		if(x->updateFlag)
			x->update(timeInterval);
			
	for(auto x: sceneComponents)
		if(x->updateFlag)
			x->update(timeInterval);
}
/////////////////////////////////////////////////////////////

void World::garbageCollect()
{
	for(auto it = scenes.begin(); it != scenes.end(); )//check scenes
	{
		it++;
		if((*std::prev(it))->toDestroy)
			destroyScene(*std::prev(it));
	}
	for(auto it = sceneObjects.begin(); it != sceneObjects.end(); )//check sceneObjects
	{
		it++;
		if((*std::prev(it))->toDestroy)
			destroySceneObject(*std::prev(it));
	}
	for(auto it = sceneComponents.begin(); it != sceneComponents.end(); )//check sceneComponents
	{
		it++;
		if((*std::prev(it))->toDestroy)
			destroySceneComponent(*std::prev(it));
	}
}

//////////////////////////////////////////////////////////////

void World::render()
{
	window->clear();

	for(auto x : sceneComponents)
		if(x->toDraw)
			window->draw(*x);
		
		
	window->display();
}


////////////////////////////////////////////////////////////////////////
void World::onGameStart()
{
	Scene *scene = createScene();
	
	
	Player *rect = scene->spawnObject<Player>();
	SceneComponent *spr1 = new SpriteComponent();
	static_cast<SpriteComponent*>(spr1)->openTexture("sprite.gif");
	rect->addSceneComponent(spr1);
	spr1->registerComponent();
	rect->rootComponent = spr1;
	rect->rootComponent->setPosition(100,100);
	
	SpriteComponent *spr3 = new SpriteComponent();
	spr3->openTexture("sprite.gif");
	spr1->addChild(spr3);
	spr3->registerComponent();
	spr3->move(-5, -100);
	spr3->blocking = 1;
	spr3->overlapping = 0;
	
	
	Character *ch = scene->spawnObject<Character>();
	SpriteComponent *spr2 = new SpriteComponent();
	spr2->openTexture("sprite.gif");
	ch->addSceneComponent(spr2);
	spr2->registerComponent();
	ch->rootComponent = spr2;
	ch->rootComponent->setPosition(250, 250);
	
	SceneObject *bounds = scene->spawnObject<SceneObject>();
	Line *line1 = new Line(sf::Vector2f(20, 200), sf::Vector2f(200, 200));
	Line *line2 = new Line(sf::Vector2f(300, 300), sf::Vector2f(30, 350));
	Line *line3 = new Line(sf::Vector2f(300, 300), sf::Vector2f(330, 200));
	Line *line4 = new Line(sf::Vector2f(400, 100), sf::Vector2f(400, 400));
	
	bounds->addSceneComponent(line1);
	line1->registerComponent();
	bounds->addSceneComponent(line2);
	line2->registerComponent();
	bounds->addSceneComponent(line3);
	line3->registerComponent();
	bounds->addSceneComponent(line4);
	line4->registerComponent();
}
